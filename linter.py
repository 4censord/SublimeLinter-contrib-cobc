from SublimeLinter.lint import Linter, STREAM_STDERR


class cobc(Linter):
    cmd = "cobc -fsyntax-only -Wextra ${file_on_disk}"
    # regex = r":(?P<line>\d+): ((?P<error>error)|(?P<warning>warning)): (?P<message>.+)"
    regex = (
        r".*?:(?P<line>\d+): ((?P<error>error)|(?P<warning>warning)): (?P<message>.+)"
    )
    multiline = False
    defaults = {"selector": "source.cobol"}
    tempfile_suffix = "-"
    error_stream = STREAM_STDERR
